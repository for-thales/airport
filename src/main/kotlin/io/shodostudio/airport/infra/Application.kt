package io.shodostudio.airport.infra


import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.shodostudio.airport.domain.LandingPriorityService
import io.shodostudio.airport.domain.model.Angle
import io.shodostudio.airport.domain.model.Distance
import io.shodostudio.airport.domain.model.Position
import io.shodostudio.airport.domain.spi.stubs.FakePrimaryRadar
import io.shodostudio.airport.infra.rest.LandingController

class Application {
    val runner: Javalin = Javalin.create().routes {
        val radar = FakePrimaryRadar()
        val service = LandingPriorityService({ Position(
            Angle.inDegrees(49.009691),
            Angle.inDegrees(2.547925),
            Distance.inMeters(3_000.0)
        ) }, radar)
        val controller = LandingController(service)
        path("landing") {
            get(controller::getNext)
        }
    }
}


fun main() {
    val application = Application()
    application.runner.start(8080)
}
