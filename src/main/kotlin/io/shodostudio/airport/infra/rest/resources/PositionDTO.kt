package io.shodostudio.airport.infra.rest.resources

data class PositionDTO(val lat: Double, val lon: Double)