package io.shodostudio.airport.infra.rest.resources

data class AircraftDTO(val id: String, val type: String, val position: PositionDTO)