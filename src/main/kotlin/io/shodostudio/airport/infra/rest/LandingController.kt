package io.shodostudio.airport.infra.rest

import io.javalin.http.Context
import io.javalin.http.HttpCode
import io.javalin.plugin.json.JavalinJackson
import io.shodostudio.airport.domain.LandingPriorityService

class LandingController(val service: LandingPriorityService) {
    fun getNext(context: Context) {
        service.nextLanding().ifPresentOrElse( {
            context.status(HttpCode.FOUND)
            val body = JavalinJackson().toJsonString(it.toDTO())
            context.result(body)
        }, {
            context.status(HttpCode.NOT_FOUND)
        })
    }
}

