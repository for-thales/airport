package io.shodostudio.airport.domain.spi

import io.shodostudio.airport.domain.model.Aircraft

interface PrimaryRadar {
    fun approaching(): List<Aircraft>
}