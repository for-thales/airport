package io.shodostudio.airport.domain.spi.stubs

import io.shodostudio.airport.domain.model.Aircraft
import io.shodostudio.airport.domain.spi.PrimaryRadar

class FakePrimaryRadar: PrimaryRadar {
    private val detections = mutableListOf<Aircraft>()
    fun detect(aircraft: Aircraft) = detections.add(aircraft)

    override fun approaching(): List<Aircraft> = detections

}