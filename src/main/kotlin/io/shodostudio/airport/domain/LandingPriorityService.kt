package io.shodostudio.airport.domain

import io.shodostudio.airport.domain.api.GetNextLandingAircraft
import io.shodostudio.airport.domain.model.Aircraft
import io.shodostudio.airport.domain.model.Distance
import io.shodostudio.airport.domain.model.Position
import io.shodostudio.airport.domain.spi.PrimaryRadar
import java.util.*

class LandingPriorityService(val airportPosition: () -> Position, val primary: PrimaryRadar): GetNextLandingAircraft {
    override fun nextLanding(): Optional<Aircraft> {
        val aircrafts = primary.approaching().sortedBy { distanceToAirport(it).inMeters() }
        val biggestCloseToAirport = aircrafts
            .filter { distanceToAirport(it) < Distance.inMeters(10_000.0) }.sortedBy { it.tonnage() }

        if (biggestCloseToAirport.isNotEmpty() && !biggestCloseToAirport.hasSameTonnage()) {
            return Optional.of(biggestCloseToAirport.last())
        }

        return Optional.of(aircrafts.firstOrNull() ?: return Optional.empty())
    }

    private fun distanceToAirport(aircraft: Aircraft): Distance = airportPosition().distanceTo(aircraft.position)
}

    private fun List<Aircraft>.hasSameTonnage(): Boolean {
        return this.map { it.tonnage() }.distinct().size == 1
    }
