package io.shodostudio.airport.domain.model

class Aircraft(
    val id: String,
    val type: AircraftType,
    val position: Position,
) {
    override fun equals(other: Any?): Boolean {
        return (other is Aircraft) && id == other.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    fun tonnage() = type.tonnage
}