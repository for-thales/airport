package io.shodostudio.airport.domain.model

import kotlin.math.PI

data class Angle private constructor(private val value: Double) {
    fun inRadians(): Double {
        return value
    }

    fun inDegrees(): Double {
        return value  * 180.0 / PI
    }

    operator fun plus(other: Angle): Angle {
        return Angle.inRadians(inRadians() + other.inRadians())
    }

    companion object {
        val Invalid = Angle(181.0)

        fun inRadians(value: Double): Angle {
            return Angle(value)
        }

        fun inDegrees(value: Double): Angle {
            return Angle(value * PI / 180)
        }
    }
}