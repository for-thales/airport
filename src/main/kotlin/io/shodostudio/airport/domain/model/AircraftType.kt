package io.shodostudio.airport.domain.model

enum class AircraftType(val tonnage: Double) {
    CESSNA(15.03),
    A320(37.40);

}