package io.shodostudio.airport.domain.model

import kotlin.math.abs

data class Distance private constructor(private val value: Double) {
    fun inMeters(): Double {
        return value
    }

    operator fun compareTo(other: Distance): Int {
        return value.compareTo(other.value)
    }

    override fun equals(other: Any?): Boolean {
        return other is Distance && abs(value - other.value) < 10.0
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }

    companion object {
        val Invalid = Distance(384_400_000.0)

        fun inMeters(value: Double): Distance {
            return Distance(value)
        }
    }
}