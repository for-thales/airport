package io.shodostudio.airport.domain.model

import kotlin.math.*


data class Position(
    val latitude: Angle,
    val longitude: Angle,
    val altitude: Distance,
) {
    fun distanceTo(other: Position): Distance {
        val d = acos(
            sin(latitude.inRadians()) * sin(other.latitude.inRadians()) + cos(latitude.inRadians()) * cos(other.latitude.inRadians()) * cos(
                other.longitude.inRadians() - longitude.inRadians()
            )
        ) * 6371_000
        val ht = altitude.inMeters() - other.altitude.inMeters()
        return Distance.inMeters(sqrt(d.pow(2) + ht.pow(2)))
    }

    operator fun plus(d: Distance): Position {
        val lat = Angle.inDegrees(latitude.inDegrees() + d.inMeters() / 111_200)
        return Position(lat, longitude, altitude)
    }

    companion object {
        val Null = Position(Angle.Invalid, Angle.Invalid, Distance.inMeters(3_000.0))
    }
}