package io.shodostudio.airport.domain.api

import io.shodostudio.airport.domain.model.Aircraft
import java.util.*

interface GetNextLandingAircraft {
    fun nextLanding(): Optional<Aircraft>
}