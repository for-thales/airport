package io.shodostudio.airport.domain

import io.shodostudio.airport.domain.model.Angle
import io.shodostudio.airport.domain.model.Distance
import io.shodostudio.airport.domain.model.Position
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset
import org.junit.jupiter.api.Test


class PositionTest {
    @Test
    fun `assert equality`() {
        val p1 = Position(
            latitude= Angle.inDegrees(49.009691),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        )
        val p2 = Position(
            latitude= Angle.inDegrees(49.009691),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        )

        assertThat(p1).isEqualTo(p2)
        assertThat(p2).isEqualTo(p1)
    }

    @Test
    fun `assert hashcode equality`() {
        val p1 = Position(
            latitude= Angle.inDegrees(49.009691),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        )
        val p2 = Position(
            latitude= Angle.inDegrees(49.009691),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        )

        assertThat(p1.hashCode()).isEqualTo(p2.hashCode())
        assertThat(p2.hashCode()).isEqualTo(p1.hashCode())
    }

    @Test
    fun `assert inequality`() {
        val p1 = Position(
            latitude= Angle.inDegrees(49.009691),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        )
        val p3 = Position(
            latitude= Angle.inDegrees(50.009691),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        )
        val p4 = Position(
            latitude= Angle.inDegrees(49.009691),
            longitude= Angle.inDegrees(3.547925),
            altitude = Distance.inMeters(3_000.0)
        )
        assertThat(p1.hashCode()).isNotEqualTo(p3.hashCode())
        assertThat(p1.hashCode()).isNotEqualTo(p4.hashCode())
    }

    @Test
    fun `should calculate the distance between two positions`() {
        val p1 = Position(latitude= Angle.inDegrees(49.0), longitude= Angle.inDegrees(0.0), altitude = Distance.inMeters(3_000.0))
        val p2 = Position(latitude= Angle.inDegrees(49.1), longitude= Angle.inDegrees(0.0), altitude = Distance.inMeters(3_000.0))

        assertThat(p1.distanceTo(p2)).isEqualTo(Distance.inMeters(11_120.0))
    }

    @Test
    fun `should sum distance`() {
        val p1 = Position(latitude= Angle.inDegrees(0.0), longitude= Angle.inDegrees(0.0), altitude = Distance.inMeters(3_000.0))
        val p2 = p1 + Distance.inMeters(10_000.0)

        assertThat(p1.distanceTo(p2).inMeters()).isCloseTo(10_000.0, Offset.offset(10.0))
    }

    @Test
    fun `assert null object`() {

        val p1 = Position(
            latitude= Angle.inDegrees(49.009691),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        )

        assertThat(Position.Null).isEqualTo(Position.Null)
        assertThat(p1).isNotEqualTo(Position.Null)
        assertThat(Position.Null).isNotEqualTo(p1)

    }
}

