package io.shodostudio.airport.domain

import io.shodostudio.airport.domain.model.*
import io.shodostudio.airport.domain.spi.stubs.FakePrimaryRadar
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*

class LandingPriorityServiceTest {

    /*
    Degrees de CDG	49.009691
    Longitude de CDG	2.547925
    */

    @Test
    fun `no next landing when no airplane approaching`() {
        val radar = FakePrimaryRadar()
        val service = LandingPriorityService({Position(
            Angle.inDegrees(49.009691),
            Angle.inDegrees(2.547925),
            Distance.inMeters(3_000.0)
        )}, radar)

        assertThat(service.nextLanding()).isEqualTo(Optional.empty<Aircraft>())
    }

    @Test
    fun `next landing when an airplane approaching`() {
        val aircraft = Aircraft("cessna-1", AircraftType.CESSNA, Position(
            latitude= Angle.inDegrees(49.009691),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        ))
        val radar = FakePrimaryRadar()
        radar.detect(aircraft)
        val service = LandingPriorityService({ Position(
            Angle.inDegrees(49.009691),
            Angle.inDegrees(2.547925),
            Distance.inMeters(3_000.0)
        ) }, radar)

        assertThat(service.nextLanding()).isEqualTo(Optional.of(aircraft))
    }

    @Test
    fun `next landing should be the closest approaching airplane`() {
        val aircraft1 = Aircraft("cessna-1", AircraftType.CESSNA, Position(
            latitude= Angle.inDegrees(49.009791),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        ))
        val aircraft2 = Aircraft("cessna-2", AircraftType.CESSNA, Position(
            latitude= Angle.inDegrees(49.009681),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        ))
        val radar = FakePrimaryRadar()
        radar.detect(aircraft2)
        radar.detect(aircraft1)
        val service = LandingPriorityService({ Position(
            Angle.inDegrees(49.009691),
            Angle.inDegrees(2.547925),
            Distance.inMeters(3_000.0)
        ) }, radar)

        assertThat(service.nextLanding()).isEqualTo(Optional.of(aircraft2))
    }


    // lat 0.1 = 11km
    @Test
    fun `next landing should be the biggest approaching airplane in 10km range`() {
        val aircraft1 = Aircraft("cessna-1", AircraftType.CESSNA, Position(
            latitude= Angle.inDegrees(49.009691),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        ))
        val aircraft2 = Aircraft("A320-1", AircraftType.A320, Position(
            latitude= Angle.inDegrees(49.095000),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        ))
        val radar = FakePrimaryRadar()
        radar.detect(aircraft2)
        radar.detect(aircraft1)
        val service = LandingPriorityService({ Position(
            Angle.inDegrees(49.009691),
            Angle.inDegrees(2.547925),
            Distance.inMeters(3_000.0)
        ) }, radar)

        assertThat(service.nextLanding()).isEqualTo(Optional.of(aircraft2))
    }
}