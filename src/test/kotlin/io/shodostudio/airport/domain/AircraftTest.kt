package io.shodostudio.airport.domain

import io.shodostudio.airport.domain.model.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.Test




class AircraftTest {

    /*
    Degrees de CDG	49.009691
    Longitude de CDG	2.547925
     */

    @Test
    fun `assert equality`() {
        val aircraft1 = Aircraft("cessna-1", AircraftType.CESSNA, Position(
            latitude=  Angle.inDegrees(49.009691),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        ))
        val aircraft2 = Aircraft("cessna-1", AircraftType.CESSNA, Position(
            latitude=  Angle.inDegrees(49.109691),
            longitude= Angle.inDegrees(2.747925),
            altitude = Distance.inMeters(3_000.0)
        ))

        assertThat(aircraft1).isEqualTo(aircraft2)
        assertThat(aircraft2).isEqualTo(aircraft1)
    }

    @RepeatedTest(150)
    fun `assert inequality`() {
        val aircraft1 = Aircraft("cessna-1", AircraftType.CESSNA, Position(
            latitude= Angle.inDegrees(49.009691),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        ))
        val aircraft3 = Aircraft("cessna-2", AircraftType.CESSNA, Position(
            latitude= Angle.inDegrees(49.209691),
            longitude= Angle.inDegrees(2.347925),
            altitude = Distance.inMeters(3_000.0)
        ))

        assertThat(aircraft1).isNotEqualTo(aircraft3)
        assertThat(aircraft3).isNotEqualTo(aircraft1)
    }

    @Test
    fun `assert hashcode equality`() {
        val aircraft1 = Aircraft("cessna-1", AircraftType.CESSNA, Position(
            latitude= Angle.inDegrees(49.009691),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        ))
        val aircraft2 = Aircraft("cessna-1", AircraftType.CESSNA, Position(
            latitude= Angle.inDegrees(49.109691),
            longitude= Angle.inDegrees(2.747925),
            altitude = Distance.inMeters(3_000.0)
        ))

        assertThat(aircraft1.hashCode()).isEqualTo(aircraft2.hashCode())
        assertThat(aircraft2.hashCode()).isEqualTo(aircraft1.hashCode())
    }

    @Test
    fun `assert hashcode inequality`() {
        val aircraft1 = Aircraft("cessna-1", AircraftType.CESSNA, Position(
            latitude= Angle.inDegrees(49.009691),
            longitude= Angle.inDegrees(2.547925),
            altitude = Distance.inMeters(3_000.0)
        ))
        val aircraft3 = Aircraft("cessna-2", AircraftType.CESSNA, Position(
            latitude= Angle.inDegrees(49.209691),
            longitude= Angle.inDegrees(2.347925),
            altitude = Distance.inMeters(3_000.0)
        ))

        assertThat(aircraft1.hashCode()).isNotEqualTo(aircraft3.hashCode())
        assertThat(aircraft3.hashCode()).isNotEqualTo(aircraft1.hashCode())
    }
}